//
//  WSImageView.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 12/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataDownloader.h"

@interface WSImageView : UIImageView <DataDownloaderDelegate>

@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) DataDownloader *dataDownloader;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

- (id)initWithFrame:(CGRect)frame withURL:(NSURL *)url;

@end

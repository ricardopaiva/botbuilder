//
//  ClickableWebImageView.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "WebImageView.h"

@protocol ClickableWebImageDelegate <NSObject>
@optional
-(void) didTapImageWithTag:(NSInteger)tag;
@end

@interface ClickableWebImageView : WebImageView
@property (strong, nonatomic) id<ClickableWebImageDelegate> delegate;

-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url delegate:(id<ClickableWebImageDelegate>)delegate;
-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url delegate:(id<ClickableWebImageDelegate>)delegate operationQueue:(NSOperationQueue *)queue;

@end

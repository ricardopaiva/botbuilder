//
//  BotView.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bot.h"
#import "WSImageView.h"

@interface BotView : UIView

@property (strong, nonatomic) Bot *bot;
@property (strong, nonatomic) WSImageView *fullBotView;

- (id)initWithFrame:(CGRect)frame withBot:(Bot *)bot;
- (void)updateBotWithURL:(NSURL *)url;

-(void)startActivityIndicator;
-(void)stopActivityIndicator;

@end

//
//  FileDownloader.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FileDownloaderDelegate <NSObject>
@required
- (void) didFinishedDownloadingFileWithPath:(NSString *)filePath;
@optional
- (void) didFailedDownloadingFile:(NSError *)error;
- (void) didReceiveFileDataWithPercentage:(float)percentage;
@end

@interface FileDownloader : NSOperation <NSURLConnectionDataDelegate, NSURLConnectionDelegate>
@property (strong, nonatomic) NSObject<FileDownloaderDelegate> *delegate;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSFileHandle *fileHandler;

- (id) initWithRequest:(NSURLRequest *)request delegate:(id<FileDownloaderDelegate>)delegate;
- (void) startDownload;
- (void) stopDownload;

@end

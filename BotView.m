//
//  BotView.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "BotView.h"
#import "WebServiceHelper.h"

@interface BotView()
{
    UIActivityIndicatorView  *activityIndicator;
}
@end

@implementation BotView

- (id)initWithFrame:(CGRect)frame withBot:(Bot *)bot
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBot:bot];
        
        //Init activityIndicator
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
        [activityIndicator setCenter:self.center];
        [self addSubview:activityIndicator];
    }
    return self;
}

- (void)updateBotWithURL:(NSURL *)url
{
    if (self.fullBotView) {
        [self.fullBotView removeFromSuperview];
    }
    self.fullBotView = [[WSImageView alloc] initWithFrame:self.frame withURL:url];
    [self insertSubview:self.fullBotView atIndex:0];
}

#pragma mark - Spinner Functions

-(void)startActivityIndicator
{
    [activityIndicator startAnimating];
}

-(void)stopActivityIndicator
{
    [activityIndicator stopAnimating];
}

@end

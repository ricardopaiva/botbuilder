//
//  LoginManager.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebServiceHelper.h"
#import "DataDownloader.h"

typedef enum
{
    STATE_LOGGEDOUT = 0,
    STATE_LOGGEDIN = 1
} LogState;

@protocol LoginManagerDelegate <NSObject>

@required
-(void)didChangeLogState:(LogState)state;
@optional
-(void)didFailedLogin:(NSError *)error;

@end

@interface LoginManager : NSObject <DataDownloaderDelegate>

@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) id<LoginManagerDelegate> delegate;

-(LogState) getLogState;
-(void)logout;
-(void)loginWithUser:(NSString *)user andPassword:(NSString *)pass onWebservice:(WebServiceHelper *)ws;

@end

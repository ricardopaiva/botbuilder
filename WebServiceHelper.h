//
//  WebServiceHelper.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

#define mediaWSAddress @"https://codebits.eu"

@interface WebServiceHelper : NSObject

@property (strong, nonatomic) NSString *wsBaseAddress;
@property (readwrite, nonatomic) NSURLCacheStoragePolicy cachePolicy;
@property (readwrite, nonatomic) NSTimeInterval timeout;

#pragma mark - Authentication Services

- (NSURLRequest *) requestAuthTokenForUser:(NSString *)user withPass:(NSString *)pass;

#pragma mark - User Info Services

- (NSURLRequest *) requestUserInfoWithID:(NSInteger)userID withToken:(NSString *)token;

#pragma mark - Bot Services

- (NSURLRequest *) requestBotParts;
- (NSURL *) requestBotPartsURL;
- (NSURL *) requestURLToBotWithBodyID:(NSString *)bodyID withBackgroundID:(NSString *)bgcolorID withGradientID:(NSString *)gradID withEyesID:(NSString *)eyesID withMouthID:(NSString *)mouthID withLegsID:(NSString *)legsID withHeadID:(NSString *)headID withArmsID:(NSString *)armsID withBalloonText:(NSString *)balloonText;

@end

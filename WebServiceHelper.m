//
//  WebServiceHelper.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "WebServiceHelper.h"
#import "Utils.h"

@implementation WebServiceHelper

-(id)init
{
    self = [super init];
    
    if (self) {
        self.wsBaseAddress = @"https://services.sapo.pt/Codebits/";
        self.cachePolicy = NSURLCacheStorageNotAllowed;
        self.timeout = 30;
    }
    
    return self;
}

- (NSURLRequest *) requestWithURL:(NSURL *)url
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url
                                                  cachePolicy:self.cachePolicy
                                              timeoutInterval:self.timeout];
    
    return request;
}

#pragma mark - Authentication Service

- (NSURLRequest *) requestAuthTokenForUser:(NSString *)user withPass:(NSString *)pass
{
    NSString *strRequest = [[NSString alloc] initWithFormat:@"%@gettoken?user=%@&password=%@",
                            self.wsBaseAddress,user, pass];
    
    return [self requestWithURL:[NSURL URLWithString:strRequest]];
}

#pragma mark - User Info Service

- (NSURLRequest *)requestUserInfoWithID:(NSInteger)userID withToken:(NSString *)token
{
    NSString *requestStr = [[NSString alloc] initWithFormat:@"%@user/%d?token=%@",
                            self.wsBaseAddress,userID, token];
    
    return [self requestWithURL:[NSURL URLWithString:requestStr]];
}

#pragma mark - Bot Services

- (NSURLRequest *)requestBotParts
{
    NSString *strRequest = [[NSString alloc] initWithFormat:@"%@botparts", self.wsBaseAddress];
    
    return [self requestWithURL:[NSURL URLWithString:strRequest]];
}

- (NSURL *)requestBotPartsURL
{
    NSString *strRequest = [[NSString alloc] initWithFormat:@"%@botparts", self.wsBaseAddress];
    
    return [NSURL URLWithString:strRequest];
}

- (NSURL *) requestURLToBotWithBodyID:(NSString *)bodyID withBackgroundID:(NSString *)bgcolorID withGradientID:(NSString *)gradID withEyesID:(NSString *)eyesID withMouthID:(NSString *)mouthID withLegsID:(NSString *)legsID withHeadID:(NSString *)headID withArmsID:(NSString *)armsID withBalloonText:(NSString *)balloonText
{
    NSString *strRequest = [[NSString alloc] initWithFormat:@"%@botmake/%@,%@,%@,%@,%@,%@,%@,%@,%@", self.wsBaseAddress, bodyID, bgcolorID, gradID, eyesID, mouthID, legsID, headID, armsID, [Utils encodeURL:balloonText]];
    
    return [NSURL URLWithString:strRequest];
}

@end

//
//  BotBodyParts.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BotParts : NSObject

@property (strong, nonatomic) NSArray *bodies;
@property (strong, nonatomic) NSArray *backgrounds;
@property (strong, nonatomic) NSArray *gradients;
@property (strong, nonatomic) NSArray *eyes;
@property (strong, nonatomic) NSArray *mouths;
@property (strong, nonatomic) NSArray *legs;
@property (strong, nonatomic) NSArray *heads;
@property (strong, nonatomic) NSArray *arms;

@end

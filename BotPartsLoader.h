//
//  BotPartsLoader.h
//  BotBuilder
//
//  Loader class using NSOperation instead of simple NSURLConnection (DataDownloader)
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BotParts.h"

@protocol BotPartsLoaderDelegate <NSObject>
@required
- (void) didFinishedLoadingBotParts:(BotParts *)botParts;
@optional
- (void) didFailedLoadingBotParts:(NSError *)error;
@end

@interface BotPartsLoader : NSOperation
@property (strong, nonatomic) NSObject <BotPartsLoaderDelegate> *delegate;
@property (strong, nonatomic) NSURL *loadURL;

- (id) initWithURL:(NSURL *)url delegate:(id<BotPartsLoaderDelegate>)delegate;

@end

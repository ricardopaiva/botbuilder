//
//  LoginViewController.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
{
    KeychainItemWrapper *keychain;
    LoginManager *loginManager;
    WebServiceHelper *webservice;
    
    NSString *btnLoginTitle;
    NSString *btnLogoutTitle;

    NSString *user;
    NSString *pass;
}
@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.spinner setHidden:YES];
    [self.lblLoading setHidden:YES];
    
    btnLoginTitle = NSLocalizedString(@"BTN_LOGIN_TITLE", nil);
    btnLogoutTitle = NSLocalizedString(@"BTN_LOGOUT_TITLE", nil);
    
    [self updateUI:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTxtUser:nil];
    [self setTxtPassword:nil];
    [self setBtnClose:nil];
    [self setSpinner:nil];
    [self setLblLoading:nil];
    [self setBtnLogin:nil];
    [self setLblUser:nil];
    [self setLblPass:nil];
    [self setLblWelcomeName:nil];
    [super viewDidUnload];
}

-(void)setLoginManager:(LoginManager *)pLoginManager andKeyChain:(KeychainItemWrapper *)pKeyChain andWebservice:(WebServiceHelper *)pWebservice {
    loginManager = pLoginManager;
    keychain = pKeyChain;
    webservice = pWebservice;
    
    [loginManager setDelegate:self];
    
//    [self checkForCredentials];
}

//- (void)checkForCredentials
//{
//    NSString *username = [keychain objectForKey:CFBridgingRelease(kSecAttrAccount)];
//    NSString *password = [keychain objectForKey:CFBridgingRelease(kSecValueData)];
//    
//    //If user and pass are saved, tries to login using them
//    if (username.length != 0) {
//        [loginManager loginWithUser:username andPassword:password onWebservice:webservice];
//    }
//}

- (IBAction)btnDoneClick:(id)sender {
    if ([loginManager getLogState] == STATE_LOGGEDOUT)
    {
        [self performLogin];
    } else {
        [self performLogout];
    }
}

- (IBAction)btnCloseClick:(id)sender
{
    if ([[self delegate] respondsToSelector:@selector(loggedWithLoginManager:andKeychain:andWebservice:)]) {
        [[self delegate] loggedWithLoginManager:loginManager andKeychain:keychain andWebservice:webservice];
    }
    [self dismiss];
}

-(void)dismiss
{
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self dismissModalViewControllerAnimated:YES];
    }
}

-(void)reset
{
    [self.txtUser setText:nil];
    [self.txtPassword setText:nil];
}

-(void)updateUI:(BOOL)animated
{
    NSTimeInterval duration = 0.0;
    if (animated) {
        duration = 1.0;
    }
        
    [UIView animateWithDuration:duration animations:^{
        if ([loginManager getLogState] == STATE_LOGGEDIN) {
            [self.btnLogin setTitle:btnLogoutTitle];
            [self.lblUser setText:NSLocalizedString(@"LBL_USER_LOGGED", nil)];
            [self.lblPass setAlpha:0.0];
       
            NSString *username = [keychain objectForKey:CFBridgingRelease(kSecAttrAccount)];
            [self.txtUser setText:username];
            [self.txtUser setEnabled:NO];
            [self.txtPassword setAlpha:0.0];
            
            [self.lblWelcomeName setText:nil];
            [self.lblWelcomeName setAlpha:0.0];

            [self downloadLoggedUserInfo];

        } else {
            [self.btnLogin setTitle:btnLoginTitle];
            [self.lblUser setText:NSLocalizedString(@"LBL_USERNAME", nil)];
            [self.lblPass setAlpha:1.0];
            [self.txtUser setAlpha:1.0];
            [self.txtUser setText:nil];
            [self.txtPassword setAlpha:1.0];
            [self.txtUser setEnabled:YES];
            
            [self.lblWelcomeName setText:nil];
            [self.lblWelcomeName setAlpha:0.0];

        }

    }];
}

-(void)showInfoFields:(UserInfo *)userInfo
{
    [UIView animateWithDuration:1.0 animations:^{
        [self.lblWelcomeName setAlpha:1.0];
        [self.lblWelcomeName setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"LBL_WELCOME", nil), userInfo.name]];
    }];
}

-(void)downloadLoggedUserInfo
{
    NSURLRequest *req = [webservice requestUserInfoWithID:[loginManager.userID intValue] withToken:loginManager.token ];
    UserInfoLoader *userInfoLoader = [[UserInfoLoader alloc] initWithRequest:req delegate:self];
    [userInfoLoader startLoading];
}

#pragma mark - Login Functions

-(void)performLogin
{
    if ([self.txtUser isFirstResponder]) {
        [self.txtUser resignFirstResponder];
    }
    if ([self.txtPassword isFirstResponder]) {
        [self.txtPassword resignFirstResponder];
    }
    
    if ([[self.txtUser text] length] == 0)
    {
        UIAlertView *errorAlertview = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil) message:NSLocalizedString(@"ALERT_ERROR_INVALID_USER_MSG", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles: nil];
        
        [errorAlertview show];
        return;
    }
    
    user = [self.txtUser text];
    pass = [self.txtPassword text];
    
    [loginManager loginWithUser:user andPassword:pass onWebservice:webservice];
    [self startSpinner];
    
    [self reset];
}

-(void)performLogout
{
    [keychain resetKeychainItem];
    [loginManager logout];
    
    [self updateUI:YES];
    [self reset];
}

#pragma mark - Spinner Controlling Functions

-(void)startSpinner {
    [self.spinner startAnimating];
    [self.spinner setHidden:NO];
    [self.lblLoading setHidden:NO];
}

-(void)stopSpinner {
    [self.spinner stopAnimating];
    [self.spinner setHidden:YES];
    [self.lblLoading setHidden:YES];
}

#pragma mark - LoginManager Delegate Methods

-(void)didChangeLogState:(LogState)state
{
    [keychain setObject:user forKey:CFBridgingRelease(kSecAttrAccount)];
    [keychain setObject:pass forKey:CFBridgingRelease(kSecValueData)];
  
    [self stopSpinner];

    [self updateUI:YES];
    
    //If logged in, download user info
    if (state == STATE_LOGGEDIN)
    {
        [self downloadLoggedUserInfo];
    }
}

-(void)didFailedLogin:(NSError *)error
{
    if ([error.domain isEqualToString:@"auth failed"]) {
        UIAlertView *authFailedAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"ERROR", nil)
                                        message:NSLocalizedString(@"ALERT_ERROR_AUTH_FAILED", nil) delegate:nil
                                        cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles: nil];
        [authFailedAlert show];
        [self stopSpinner];
    }
}

#pragma mark - TextField Delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.txtUser]) {
        [self.txtPassword becomeFirstResponder];
    }

    if ([textField isEqual:self.txtPassword]) {
        [self performLogin];
    }
    return YES;
}

#pragma mark - UserInfoLoader Delegate methods

- (void) didFinishedLoadingUserInfo:(UserInfo *)userInfo
{
    [self showInfoFields:userInfo];
}

@end

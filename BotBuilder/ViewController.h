//
//  ViewController.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "ClickableLabel.h"
#import "BotPartsLoader.h"
#import "ClickableWebImageView.h"
#import "LoginManager.h"

@interface ViewController : UIViewController <LoginViewControllerDelegate, ClickableLabelDelegate, BotPartsLoaderDelegate, ClickableWebImageDelegate>

@end

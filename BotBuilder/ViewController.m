//
//  ViewController.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "ViewController.h"
#import "Utils.h"
#import "BotPart.h"
#import "BotView.h"
#import "Bot.h"
#import "KeychainItemWrapper.h"
#import "Reachability.h"
#import "FileDownloaderOperation.h"

typedef void (^CompletionBlock)(void);

#define bodyIndex 0
#define backgroundIndex 1
#define gradientIndex 2
#define eyesIndex 3
#define mouthIndex 4
#define legsIndex 5
#define headIndex 6
#define armsIndex 7
#define balloonIndex 8

@interface ViewController ()
{
    NSString *btnLoginTitle;
    NSString *btnLogoutTitle;
    
    NSOperationQueue *queue;
}

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
@property (weak, nonatomic) IBOutlet UIScrollView *partsScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *partsDetailScrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblCustomParts;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLogin;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnRefresh;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) LoginManager *loginManager;
@property (strong, nonatomic) KeychainItemWrapper *keychain;
@property (strong, nonatomic) WebServiceHelper *webservice;

@property (nonatomic) int botPartIndex; //Bot Body Part being customized
@property (strong, nonatomic) BotParts *botParts;

@property (strong, nonatomic) BotView *botView;
@property (nonatomic) BOOL isConnected;
@property (strong, nonatomic) Reachability* reachability;

- (IBAction)btnRefreshClick:(id)sender;

-(void)customizeUI;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [self initReachability];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imageDataAvailable:)
                                                 name:@"DataDownloadFinished"
                                               object:nil ] ;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(imageDataUnavailable:)
                                                 name:@"DataDownloadFailed"
                                               object:nil ] ;

    
    self.botPartIndex = -1;
    
    //Load LoginManager, KeychainWrapper and Webservice

    if (self.isConnected) {
        self.loginManager = [[LoginManager alloc] init];
    }
    self.keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"BotBuilderKeychain" accessGroup:nil];
    self.webservice = [[WebServiceHelper alloc] init];
    
    //Sets login button name so cannot be called before init loginManager.
    [self customizeUI];

    //Load bot parts from webservice - Changed BotPartsLoader to use NSOperation instead
//    NSURLRequest *botPartsReq = [self.webservice requestBotParts];
//    BotPartsLoader *botPartsLoader = [[BotPartsLoader alloc] initWithRequest:botPartsReq delegate:self];
//    [botPartsLoader startLoading];

    Bot *bot = [[Bot alloc] initWithBody:nil background:nil gradient:nil eyes:nil mouth:nil legs:nil head:nil arms:nil];
    self.botView = [[BotView alloc] initWithFrame:CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height) withBot:bot];
    [self.containerView addSubview:self.botView];

    queue = [[NSOperationQueue alloc] init];
    if (self.isConnected) {
        [self loadBotParts];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setContainerView:nil];
    [self setLblCustomParts:nil];
    [self setBtnRefresh:nil];
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"LoginViewController"]) {
        if ([[segue destinationViewController] respondsToSelector:@selector(setLoginManager:andKeyChain:andWebservice:)]) {

            [[segue destinationViewController] setDelegate:self];
            [[segue destinationViewController] setLoginManager:self.loginManager andKeyChain:self.keychain andWebservice:self.webservice];
        }
    }
}

- (IBAction)btnRefreshClick:(id)sender {
    if (!self.isConnected)
    {
        [self checkConnectivity];
    } else {
        [self loadBotParts];
    }
}

#pragma mark - User Functions

-(void)loadBotParts
{
    [self.botView startActivityIndicator];

    NSURL *botPartsURL = [self.webservice requestBotPartsURL];
    BotPartsLoader *botPartsLoader = [[BotPartsLoader alloc] initWithURL:botPartsURL delegate:self];
    [queue addOperation:botPartsLoader];
}

-(void)loadRandomBot
{
    [self updateBotWithPart:bodyIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.bodies.count - 1]];
    [self updateBotWithPart:backgroundIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.backgrounds.count - 1]];
    [self updateBotWithPart:gradientIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.gradients.count - 1]];
    [self updateBotWithPart:eyesIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.eyes.count - 1]];
    [self updateBotWithPart:mouthIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.mouths.count - 1]];
    [self updateBotWithPart:legsIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.legs.count - 1]];
    [self updateBotWithPart:headIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.heads.count - 1]];
    [self updateBotWithPart:armsIndex withPartID:[Utils getRandomNumberBetween:0 and:self.botParts.arms.count - 1]];

    [self updateBot];
}

-(void)customizeUI
{
    btnLoginTitle = NSLocalizedString(@"BTN_LOGIN_TITLE", nil);
    btnLogoutTitle = NSLocalizedString(@"BTN_LOGOUT_TITLE", nil);

    if ([self.loginManager getLogState] == STATE_LOGGEDIN) {
        [self.btnLogin setTitle:btnLogoutTitle];
    } else {
        [self.btnLogin setTitle:btnLoginTitle];
    }
    
    self.navBar.topItem.title = NSLocalizedString(@"APP_TITLE", nil);
    
    [self.lblCustomParts setAlpha:0.0];
    [self.partsScrollView setAlpha:0.0];
    
}

-(void)loadBotPartsInScrollView {
    
    //No Webservice available to download BotParts - Will load from pList
    NSString *partsPath = [[Utils bundlePath] stringByAppendingPathComponent:@"botParts.plist"];
    NSArray *botPartsArray = [NSArray arrayWithContentsOfFile:partsPath];
    
    int labelWidth = 70;
    int labelHeight = 20;
    int padding = 5;
    int botPartsCount = [botPartsArray count];

    [self.partsScrollView setContentSize:CGSizeMake(botPartsCount * (labelWidth + padding),
                                                      self.partsScrollView.frame.size.height)];

    for (int i = 0; i < botPartsCount; i++) {
        ClickableLabel *label = [[ClickableLabel alloc] initWithFrame:CGRectMake(i * (labelWidth + padding), 0, labelWidth, labelHeight)];
        [label setUserInteractionEnabled:YES];
        [label setTag:i];
        NSString *text = [NSString stringWithFormat:@"%@", [botPartsArray objectAtIndex:i]];
        [label setText:text];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setFont:[UIFont systemFontOfSize:13]];
        [label setAdjustsFontSizeToFitWidth:YES];
        [label setDelegate:self];
        [label setBackgroundColor:[UIColor clearColor]];
        [self.partsScrollView addSubview:label];
    }
    
    [UIView animateWithDuration:1.0 animations:^{
        [self.lblCustomParts setAlpha:1.0];
        [self.partsScrollView setAlpha:1.0];
    }];

}

-(NSArray *)getBotPartArrayToLoad:(int)partIndex
{
    self.botPartIndex = partIndex;
    
    NSArray *partArrayToLoad = nil;
    switch (partIndex) {
        case bodyIndex:
            partArrayToLoad = [self.botParts bodies];
            break;
        case backgroundIndex:
            partArrayToLoad = [self.botParts backgrounds];
            break;
        case gradientIndex:
            partArrayToLoad = [self.botParts gradients];
            break;
        case eyesIndex:
            partArrayToLoad = [self.botParts eyes];
            break;
        case mouthIndex:
            partArrayToLoad = [self.botParts mouths];
            break;
        case legsIndex:
            partArrayToLoad = [self.botParts legs];
            break;
        case headIndex:
            partArrayToLoad = [self.botParts heads];
            break;
        case armsIndex:
            partArrayToLoad = [self.botParts arms];
            break;
        default:
            break;
    }
    return partArrayToLoad;
}

-(void)loadBotPartsDetailInScrollView:(int)partIndex
{
//    self.botPartIndex = partIndex;
//
//    NSArray *partArrayToLoad = nil;
//        switch (partIndex) {
//            case bodyIndex:
//                partArrayToLoad = [self.botParts bodies];
//                break;
//            case backgroundIndex:
//                partArrayToLoad = [self.botParts backgrounds];
//                break;
//            case gradientIndex:
//                partArrayToLoad = [self.botParts gradients];
//                break;
//            case eyesIndex:
//                partArrayToLoad = [self.botParts eyes];
//                break;
//            case mouthIndex:
//                partArrayToLoad = [self.botParts mouths];
//                break;
//            case legsIndex:
//                partArrayToLoad = [self.botParts legs];
//                break;
//            case headIndex:
//                partArrayToLoad = [self.botParts heads];
//                break;
//            case armsIndex:
//                partArrayToLoad = [self.botParts arms];
//                break;
//            default:
//                break;
//        }
    
    NSArray *partArrayToLoad = [self getBotPartArrayToLoad:partIndex];
    
    int imageSize = 60;
    int padding = 5;
    int bodyPartCount = partArrayToLoad.count;
    
    for (UIView *view in [self.partsDetailScrollView subviews]) {
        if ([view isKindOfClass:[ClickableWebImageView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [self.partsDetailScrollView setContentSize:CGSizeMake(bodyPartCount * (imageSize + padding),
                                                      self.partsDetailScrollView.frame.size.height)];
    
    for (int i = 0; i < bodyPartCount; i++) {
        BotPart *botpart = [partArrayToLoad objectAtIndex:i];
        
        ClickableWebImageView *image = [[ClickableWebImageView alloc]
                                        initWithFrame:CGRectMake(i* (imageSize + padding), 0, imageSize, imageSize)
                                        withURL:[NSURL URLWithString:botpart.picker]
                                        delegate:self];
        [image setTag:i];
        [image setBackgroundColor:[UIColor lightGrayColor]];
        [self.partsDetailScrollView addSubview:image];
        
        FileDownloaderOperation *operation = [[FileDownloaderOperation alloc] initWithURL:[NSURL URLWithString:botpart.picker]];
        [operation addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
        [queue addOperation:operation]; // operation starts as soon as its added
    }
}

-(void)updateBotWithPart:(int)specificPartIndex withPartID:(int)partID
{
    BotParts *botParts = self.botParts;
    
    BotPart *body, *background, *gradient, *eyes, *mouth, *legs, *head, *arms;
    switch (specificPartIndex) {
        case bodyIndex:
            body = [botParts.bodies objectAtIndex:partID];
            [self.botView.bot setBody:body];
            break;
        case backgroundIndex:
            background = [botParts.backgrounds objectAtIndex:partID];
            [self.botView.bot setBackground:background];
            break;
        case gradientIndex:
            gradient = [botParts.gradients objectAtIndex:partID];
            [self.botView.bot setGradient:gradient];
            break;
        case eyesIndex:
            eyes = [botParts.eyes objectAtIndex:partID];
            [self.botView.bot setEyes:eyes];
            break;
        case mouthIndex:
            mouth = [botParts.mouths objectAtIndex:partID];
            [self.botView.bot setMouth:mouth];
            break;
        case legsIndex:
            legs = [botParts.legs objectAtIndex:partID];
            [self.botView.bot setLegs:legs];
            break;
        case headIndex:
            head = [botParts.heads objectAtIndex:partID];
            [self.botView.bot setHead:head];
            break;
        case armsIndex:
            arms = [botParts.arms objectAtIndex:partID];
            [self.botView.bot setArms:arms];
            break;
        default:
            break;
    }
}

-(void)updateBot {
    Bot *bot = self.botView.bot;
    NSURL *botURL = [self.webservice
                     requestURLToBotWithBodyID:bot.body.partID
                     withBackgroundID:bot.background.partID
                     withGradientID:bot.gradient.partID
                     withEyesID:bot.eyes.partID
                     withMouthID:bot.mouth.partID
                     withLegsID:bot.legs.partID
                     withHeadID:bot.head.partID
                     withArmsID:bot.arms.partID
                     withBalloonText:@""];
    [self.botView updateBotWithURL:botURL];
}

#pragma mark - Login View Controller Delegate Methods
-(void)loggedWithLoginManager:(LoginManager *)loginManager andKeychain:(KeychainItemWrapper *)keychain andWebservice:(WebServiceHelper *)webservice {
    self.loginManager = loginManager;
//    [self.loginManager setDelegate:self];
    
    self.keychain = keychain;
    self.webservice = webservice;
}

#pragma mark - ClickableLabel Delegate Methods

-(void)didTapLabelWithTag:(NSInteger)tag
{
    if (self.botPartIndex == tag) {
        return;
    }

    [self hideBotPartsDetailScrollViewWithCompletionBlock:^{
        [self loadBotPartsDetailInScrollView:tag];
        [self showBotPartsDetailScrollViewWithCompletionBlock:nil];
    } ];
}

#pragma mark - ClickableWebImageView Delegate Methods

-(void)didTapImageWithTag:(NSInteger)tag
{
    [self updateBotWithPart:self.botPartIndex withPartID:tag];
    [self updateBot];
}

#pragma mark - BotPartsLoader Delegate Methods

-(void)didFinishedLoadingBotParts:(BotParts *)botParts
{
    self.botParts = botParts;
    
    [self loadBotPartsInScrollView];  //Load labels in the parts selection scroll view

    [self.botView stopActivityIndicator];

    //Create and add random bot button
    UIBarButtonItem *randomBotBtn = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"RANDOM_BOT", nil)
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(loadRandomBot)];
    self.navBar.topItem.leftBarButtonItem = randomBotBtn;
}

-(void)didFailedLoadingBotParts:(NSError *)error
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil) message:NSLocalizedString(@"ALERT_ERROR_GET_BOT_PARTS", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) otherButtonTitles:nil];
    [alert show];

    [self.botView stopActivityIndicator];
}

#pragma mark - Parts ScrollView Showing/Hiding methods

-(void)hideBotPartsDetailScrollViewWithCompletionBlock:(CompletionBlock)block {
    [UIView animateWithDuration:0.6 animations:^(void) {
        [self.partsDetailScrollView setTransform:CGAffineTransformMakeTranslation(self.partsDetailScrollView.frame.size.width + 5, 0)];
    } completion:^(BOOL finished) {
        block();
    }];
}

-(void)showBotPartsDetailScrollViewWithCompletionBlock:(CompletionBlock)block {
    //Reset scrollview position
    [self.partsDetailScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    
    [UIView animateWithDuration:0.6 animations:^(void) {
        [self.partsDetailScrollView setTransform:CGAffineTransformMakeTranslation(0, 0)];
    } completion:^(BOOL finished) {
        if (block) {
            block();
        }
    }];
}

#pragma mark - Reachability Functions

-(void)initReachability {
    //Register Reachability Observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

    self.reachability = [Reachability reachabilityForInternetConnection];
	[self.reachability startNotifier];
    
    [self checkConnectivity];
}

-(void)checkConnectivity
{
    NetworkStatus netStatus = [self.reachability currentReachabilityStatus];
    if (netStatus == NotReachable)
    {
        [self setIsConnected:NO];
        
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil)
                                    message:NSLocalizedString(@"NO_CONNECTION", nil)
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"CANCEL", nil)
                          otherButtonTitles:nil] show];
        
    } else {
        [self setIsConnected:YES];
    }
}

- (void) reachabilityChanged:(NSNotification*) notification
{
	Reachability* r = notification.object;
	if(r.currentReachabilityStatus == NotReachable) {
        [self setIsConnected:NO];
	} else {
        [self setIsConnected:YES];
        if (!self.loginManager) {
            self.loginManager = [[LoginManager alloc] init];
        }
        if (!self.botParts) {
            [self loadBotParts];
        }
    }
}

#pragma mark -
#pragma KVO Observing
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)operation change:(NSDictionary *)change context:(void *)context {
    
    NSString *source = nil;
    NSString *fileLocalPath = nil;
    NSError *error = nil;
    if ([operation isKindOfClass:[FileDownloaderOperation class]]) {
        FileDownloaderOperation *downloadOperation = (FileDownloaderOperation *)operation;

        NSArray *partArrayToLoad = [self getBotPartArrayToLoad:self.botPartIndex];
        
        for (int i = 0; i < [partArrayToLoad count]; i++) {
            BotPart *botpart = [partArrayToLoad objectAtIndex:i];
            if ([[botpart picker] isEqualToString:[downloadOperation.connectionURL absoluteString]]) {
                source = botpart.picker;
                break;
            }
        }
        if (source) {
            fileLocalPath = [downloadOperation fileLocalPath];
            error = [downloadOperation error];
        }
    }
    if (source) {
//        NSLog(@"Downloaded finished from %@", source);
        if (error != nil) {
            // handle error
            // Notify that we have got an error downloading this data;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataDownloadFailed"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:source, @"source", error, @"error", nil]];
            
        } else {
            // Notify that we have got this source data;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataDownloadFinished"
                                                                object:self
                                                              userInfo:[NSDictionary dictionaryWithObjectsAndKeys:source, @"source", fileLocalPath, @"fileLocalPath", nil]];
        }
        
    }
}

#pragma mark - FileDownloader KVO Methods
- (void)imageDataAvailable:(NSNotification *)notification {
    NSString *source = [notification.userInfo valueForKey:@"source"];

    for (UIView *view in [self.partsDetailScrollView subviews]) {
        if ([view isKindOfClass:[ClickableWebImageView class]]) {
            ClickableWebImageView *imgView = (ClickableWebImageView *)view;

            if ([[[imgView imageURL] absoluteString] isEqualToString:source]) {
                NSString *filePath = [notification.userInfo valueForKey:@"fileLocalPath"];
                [imgView setImage:[UIImage imageWithContentsOfFile:filePath]];
                [[imgView activityIndicator] stopAnimating];
                break;
            }
        }
    }
}

- (void)imageDataUnavailable:(NSNotification *)notification {
    NSString *source = [notification.userInfo valueForKey:@"source"];
    for (UIView *view in [self.partsDetailScrollView subviews]) {
        if ([view isKindOfClass:[ClickableWebImageView class]]) {
            ClickableWebImageView *imgView = (ClickableWebImageView *)view;
            
            if ([[[imgView imageURL] absoluteString] isEqualToString:source]) {
                [[imgView activityIndicator] stopAnimating];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Download Failed" message:@"Please check internet connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                break;
            }
        }
    }
}


@end

//
//  LoginViewController.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginManager.h"
#import "KeychainItemWrapper.h"
#import "UserInfoLoader.h"

@protocol LoginViewControllerDelegate <NSObject>
@required
-(void)loggedWithLoginManager:(LoginManager *)loginManager andKeychain:(KeychainItemWrapper *)keychain andWebservice:(WebServiceHelper *)webservice;
@end

@interface LoginViewController : UIViewController <LoginManagerDelegate, UITextFieldDelegate, UserInfoLoaderDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUser;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnClose;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLogin;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UILabel *lblLoading;
@property (weak, nonatomic) IBOutlet UILabel *lblUser;
@property (weak, nonatomic) IBOutlet UILabel *lblPass;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeName;

@property (strong, nonatomic) id<LoginViewControllerDelegate> delegate;

- (IBAction)btnDoneClick:(id)sender;
- (IBAction)btnCloseClick:(id)sender;

-(void)setLoginManager:(LoginManager *)pLoginManager andKeyChain:(KeychainItemWrapper *)pKeyChain andWebservice:(WebServiceHelper *)pWebservice;

@end

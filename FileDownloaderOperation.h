//
//  FileDownloaderOperation.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 15/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileDownloaderOperation : NSOperation
{
    // In concurrent operations, we have to manage the operation's state
    BOOL executing;
    BOOL finished;
    
    // The actual NSURLConnection management
    NSURL*    connectionURL;
    NSURLConnection*  connection;
}

@property (nonatomic,readonly) NSError* error;
@property (nonatomic,readonly) NSURL *connectionURL;

@property (strong, nonatomic) NSFileHandle *fileHandler;
@property (strong, nonatomic) NSString *fileLocalPath;

- (id)initWithURL:(NSURL*)url;

@end

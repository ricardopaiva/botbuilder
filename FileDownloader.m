//
//  FileDownloader.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "FileDownloader.h"
#import "Utils.h"

@interface FileDownloader()
{
    NSString *fileLocalPath;
    NSString *tempFileLocalPath;
    float floatTotalData;
    float floatReceivedData;
}
@end

@implementation FileDownloader

- (id)initWithRequest:(NSURLRequest *)request delegate:(id<FileDownloaderDelegate>)delegate
{
    self = [super init];
    if (self) {
        [self setDelegate:delegate];
        
        self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        
        [self.connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];

        NSString *requestLastComp = [request.URL.absoluteString lastPathComponent];
        fileLocalPath = [[Utils cacheDirectoryPath] stringByAppendingPathComponent:requestLastComp];
    }
    
    return self;
}

-(void)startDownload
{
    floatTotalData = 0;
    floatReceivedData = 0;
    [self.connection start];
}

-(void)stopDownload
{
    [self reset];
}

- (void) reset
{
    [self.fileHandler closeFile];
    [self setFileHandler:nil];
    
    [self.connection cancel];
    [self setConnection:nil];
    [self setDelegate:nil];
}

#pragma mark - NSURLConnection delegate methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    floatTotalData = (float) response.expectedContentLength;
    tempFileLocalPath = [NSString stringWithFormat:@"%@%@", fileLocalPath, @".temp"];
    [Utils createEmptyFileAtPath:tempFileLocalPath];
    self.fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:tempFileLocalPath];
    [self.fileHandler seekToFileOffset:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.fileHandler seekToEndOfFile];
    [self.fileHandler writeData:data];
    
    floatReceivedData += data.length;
    
    float progress = (floatReceivedData / floatTotalData) * 100.0f;
    
    if ([self.delegate respondsToSelector:@selector(didReceiveFileDataWithPercentage:)]) {
        [self.delegate didReceiveFileDataWithPercentage:progress];
    }
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.delegate) {
        if ([Utils renameFileAtPath:tempFileLocalPath toPath:fileLocalPath]) {
            [self.delegate didFinishedDownloadingFileWithPath:fileLocalPath];
            [self reset];
        }
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(didFailedDownloadingFile:)]) {
        [self.delegate didFailedDownloadingFile:error];
    }
    [self reset];
}

@end

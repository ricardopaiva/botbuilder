//
//  Bot.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BotPart.h"

@interface Bot : NSObject

@property (strong, nonatomic) BotPart *body;
@property (strong, nonatomic) BotPart *background;
@property (strong, nonatomic) BotPart *gradient;
@property (strong, nonatomic) BotPart *eyes;
@property (strong, nonatomic) BotPart *mouth;
@property (strong, nonatomic) BotPart *legs;
@property (strong, nonatomic) BotPart *head;
@property (strong, nonatomic) BotPart *arms;

-(id)initWithBody:(BotPart *)body
       background:(BotPart *)background
         gradient:(BotPart *)gradient
             eyes:(BotPart *)eyes
            mouth:(BotPart *)mouth
             legs:(BotPart *)legs
             head:(BotPart *)head
             arms:(BotPart *)arms;

@end

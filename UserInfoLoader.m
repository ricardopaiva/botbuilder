//
//  UserInfoLoader.m
//  BotBuilder
//
//  Loader class using simple NSURLConnection
//
//  Created by Ricardo Paiva on 12/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "UserInfoLoader.h"

@implementation UserInfoLoader

-(id)initWithRequest:(NSURLRequest *)request delegate:(id<UserInfoLoaderDelegate>)delegate
{
    self = [super init];
    if (self) {
        [self setDelegate:delegate];
        self.dataDownloader = [[DataDownloader alloc] initWithRequest:request delegate:self];
    }
    return self;
}

-(void)startLoading
{
    [self.dataDownloader startDownload];
}

- (void) parseJsonToUserInfo:(NSDictionary *)json
{
    //parse json
    NSString *userID = [json valueForKey:@"id"];
    NSString *nick = [json valueForKey:@"nick"];
    NSString *avatar = [json valueForKey:@"avatar"];
    NSString *twitter = [json valueForKey:@"twitter"];
    NSString *name = [json valueForKey:@"name"];
    
    UserInfo *userInfo = [[UserInfo alloc] initWithUserID:userID withNickname:nick withAvatarImgURL:avatar withTwitter:twitter withName:name];

    // finish parsing, tell delegate
    if ([self.delegate respondsToSelector:@selector(didFinishedLoadingUserInfo:)]) {
        [self.delegate didFinishedLoadingUserInfo:userInfo];
    }
}

#pragma mark - DataDownloader Delegate methods

-(void)didFinishedDownloadData:(NSData *)data {
    NSError *error;
    
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    if (error) {
        if ([self.delegate respondsToSelector:@selector(didFailedLoadingUserInfo:)]) {
            [self.delegate didFailedLoadingUserInfo:error];
        }
    }else{
        [self parseJsonToUserInfo:jsonData];
    }
}

-(void)didFailedDownloadData:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(didFailedLoadingUserInfo:)]) {
        [self.delegate didFailedLoadingUserInfo:error];
    }
}

@end

//
//  FileDownloaderOperation.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 15/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "FileDownloaderOperation.h"
#import "Utils.h"

@interface FileDownloaderOperation()
{
    NSString *fileLocalPath;
    NSString *tempFileLocalPath;
    float floatTotalData;
    float floatReceivedData;
}
@end

@implementation FileDownloaderOperation
@synthesize error = error;
@synthesize connectionURL = connectionURL;
@synthesize fileLocalPath = fileLocalPath;

#pragma mark -
#pragma mark Initialization & Memory Management

- (id)initWithURL:(NSURL *)url
{
    self = [super init];
    if (self) {
        connectionURL = [url copy];
    }
    return self;
}

- (void)dealloc
{
    if( connection ) {
        [connection cancel];
        connection = nil;
    }
    connectionURL = nil;
    fileLocalPath = nil;
}

#pragma mark -
#pragma mark Start & Utility Methods

// This method is just for convenience. It cancels the URL connection if it
// still exists and finishes up the operation.
- (void)done
{
    [self.fileHandler closeFile];
    [self setFileHandler:nil];

    if( connection ) {
        [connection cancel];
        connection = nil;
    }

    // KVO Usage - Alert anyone that we are finished
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
    executing = NO;
    finished  = YES;
    [self didChangeValueForKey:@"isFinished"];
    [self didChangeValueForKey:@"isExecuting"];
    
}

-(void)canceled {
	// Code for being cancelled
    error = [[NSError alloc] initWithDomain:@"FileDownloaderOperation"
                                        code:123
                                    userInfo:nil];
    [self done];
}

- (void)start
{
    // Ensure that this operation starts on the main thread
    if (![NSThread isMainThread])
    {
        [self performSelectorOnMainThread:@selector(start)
                               withObject:nil waitUntilDone:NO];
        return;
    }
    
    // Ensure that the operation should exute
    if (finished || [self isCancelled]) {
        [self done];
        return;
    }
    
    // From this point on, the operation is officially executing--remember, isExecuting
    // needs to be KVO compliant!
    [self willChangeValueForKey:@"isExecuting"];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
    
    // Create the NSURLConnection--this could have been done in init, but we delayed
    // until no in case the operation was never enqueued or was cancelled before starting
    NSURLRequest *request = [NSURLRequest
                             requestWithURL:connectionURL
                             cachePolicy:NSURLRequestReloadIgnoringCacheData
                             timeoutInterval:20.0];
    connection = [[NSURLConnection alloc] initWithRequest:request
                                                 delegate:self
                                         startImmediately:NO];
    [connection scheduleInRunLoop:[NSRunLoop currentRunLoop]
                          forMode:NSRunLoopCommonModes];

    floatTotalData = 0;
    floatReceivedData = 0;
    NSString *requestLastComp = [request.URL.absoluteString lastPathComponent];
    fileLocalPath = [[Utils cacheDirectoryPath] stringByAppendingPathComponent:requestLastComp];

    [connection start];

}

-(void)stopDownload
{
    [self canceled];
}

#pragma mark -
#pragma mark Overrides

- (BOOL)isConcurrent
{
    return YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isFinished
{
    return finished;
}

#pragma mark -
#pragma mark Delegate Methods for NSURLConnection

// The connection failed
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        fileLocalPath = nil;
		[self done];
	}
}

// The connection received more data
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)receivedData
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
    
    [self.fileHandler seekToEndOfFile];
    [self.fileHandler writeData:receivedData];
    
    floatReceivedData += receivedData.length;
}

// Initial response
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
    
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSInteger statusCode = [httpResponse statusCode];
    if( statusCode == 200 ) {

        floatTotalData = (float) response.expectedContentLength;
        tempFileLocalPath = [NSString stringWithFormat:@"%@%@", fileLocalPath, @".temp"];
        [Utils createEmptyFileAtPath:tempFileLocalPath];
        self.fileHandler = [NSFileHandle fileHandleForUpdatingAtPath:tempFileLocalPath];
        [self.fileHandler seekToFileOffset:0];
        
    } else {
        NSString* statusError  = [NSString stringWithFormat:NSLocalizedString(@"HTTP Error: %ld", nil), statusCode];
        NSDictionary* userInfo = [NSDictionary dictionaryWithObject:statusError forKey:NSLocalizedDescriptionKey];
        error = [[NSError alloc] initWithDomain:@"FileDownloaderOperation"
                                            code:statusCode
                                        userInfo:userInfo];
        [self done];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    // Check if the operation has been cancelled
    if([self isCancelled]) {
        [self canceled];
		return;
    }
	else {
        [Utils renameFileAtPath:tempFileLocalPath toPath:fileLocalPath];
		[self done];
	}
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}

@end

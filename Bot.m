//
//  Bot.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "Bot.h"

@implementation Bot

-(id)initWithBody:(BotPart *)body background:(BotPart *)background gradient:(BotPart *)gradient eyes:(BotPart *)eyes mouth:(BotPart *)mouth legs:(BotPart *)legs head:(BotPart *)head arms:(BotPart *)arms
{
    self = [super init];
    if (self) {
        [self setBody:body];
        [self setBackground:background];
        [self setGradient:gradient];
        [self setEyes:eyes];
        [self setMouth:mouth];
        [self setHead:head];
        [self setLegs:legs];
        [self setArms:arms];
    }    
    return self;
}

@end

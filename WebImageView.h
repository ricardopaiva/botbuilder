//
//  WebImageView.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileDownloaderOperation.h"

@interface WebImageView : UIImageView

@property (strong, nonatomic) NSURL *imageURL;
@property (strong, nonatomic) FileDownloaderOperation *imgFileDownloader;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url;
-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url operationQueue:(NSOperationQueue *)queue;

@end

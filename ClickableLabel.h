//
//  ClickableLabel.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ClickableLabelDelegate <NSObject>
@optional
-(void) didTapLabelWithTag:(NSInteger)tag;
@end

@interface ClickableLabel : UILabel

@property (strong, nonatomic) id<ClickableLabelDelegate> delegate;

@end

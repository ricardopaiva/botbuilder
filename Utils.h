//
//  Utils.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

+ (NSString*)encodeURL:(NSString *)string;
+ (NSString *) cacheDirectoryPath;
+ (NSString *) documentDirectoryPath;
+ (NSString *) bundlePath;
+ (BOOL) createEmptyFileAtPath:(NSString *) path;
+ (BOOL) renameFileAtPath:(NSString *)path toPath:(NSString *)newPath;
+ (BOOL) fileExistsAtPath:(NSString *)path;
+(int) getRandomNumberBetween:(int)from and:(int)to;
    
@end

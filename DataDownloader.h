//
//  DataDownloader.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataDownloaderDelegate <NSObject>
@required
- (void) didFinishedDownloadData:(NSData *)data;
@optional
- (void) didFailedDownloadData:(NSError *)error;
@end

@interface DataDownloader : NSObject <NSURLConnectionDataDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) id<DataDownloaderDelegate> delegate;
@property (strong, nonatomic) NSURLConnection *connection;
@property (strong, nonatomic) NSMutableData *receivedData;

- (id) initWithRequest:(NSURLRequest *)request delegate:(id<DataDownloaderDelegate>)delegate;
- (void) startDownload;
- (void) stopDownload;

@end

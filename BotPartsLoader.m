//
//  BotPartsLoader.m
//  BotBuilder
//
//  Loader class using NSOperation instead of simple NSURLConnection (DataDownloader)
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "BotPartsLoader.h"
#import "WebServiceHelper.h"
#import "BotPart.h"

@implementation BotPartsLoader

- (id)initWithURL:(NSURL*)url delegate:(id<BotPartsLoaderDelegate>)delegate;
{
    self = [super init];
    if (self) {
        [self setDelegate:delegate];
        [self setLoadURL:url];        
    }
    return self;
}

- (void)main {
    
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfURL:self.loadURL options:NSDataReadingUncached error:&error];
    
    if (error)
    {
        if ([self.delegate respondsToSelector:@selector(didFailedLoadingBotParts:)]) {
            [self.delegate performSelectorOnMainThread:@selector(didFailedLoadingBotParts:)
                                            withObject:error
                                         waitUntilDone:YES];
            return;
        }
    }
    
    error = nil;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

    if (!jsonData) {
        if ([self.delegate respondsToSelector:@selector(didFailedLoadingBotParts:)]) {
            [self.delegate performSelectorOnMainThread:@selector(didFailedLoadingBotParts:)
                                            withObject:error
                                         waitUntilDone:YES];
            return;
        }
    }

    [self parseJsonToBodyParts:jsonData];
}

- (void) parseJsonToBodyParts:(NSDictionary *)json
{
    //parse each collection
    NSArray *bodies = [json valueForKey:@"body"];
    NSArray *backgrounds = [json valueForKey:@"bgcolor"];
    NSArray *gradients = [json valueForKey:@"grad"];
    NSArray *eyes = [json valueForKey:@"eyes"];
    NSArray *mouths = [json valueForKey:@"mouth"];
    NSArray *legs = [json valueForKey:@"legs"];
    NSArray *heads = [json valueForKey:@"head"];
    NSArray *arms = [json valueForKey:@"arms"];
    
    //save collections in a new BotParts instance
    BotParts *botParts = [[BotParts alloc] init];
    [botParts setBodies:[self getBotPartsDetailedArray:bodies]];
    [botParts setBackgrounds:[self getBotPartsDetailedArray:backgrounds]];
    [botParts setGradients:[self getBotPartsDetailedArray:gradients]];
    [botParts setEyes:[self getBotPartsDetailedArray:eyes]];
    [botParts setMouths:[self getBotPartsDetailedArray:mouths]];
    [botParts setLegs:[self getBotPartsDetailedArray:legs]];
    [botParts setHeads:[self getBotPartsDetailedArray:heads]];
    [botParts setArms:[self getBotPartsDetailedArray:arms]];

    //Execute this in the main thread
    // finish parsing, tell delegate
    if ([self.delegate respondsToSelector:@selector(didFinishedLoadingBotParts:)]) {
        [self.delegate performSelectorOnMainThread:@selector(didFinishedLoadingBotParts:)
                                        withObject:botParts
                                     waitUntilDone:YES];
    }
}

- (NSArray *) getBotPartsDetailedArray:(NSArray *)bodyParts
{
    NSMutableArray *partsMutArray = [[NSMutableArray alloc] init];
    for (NSDictionary *bodyPartDict in bodyParts) {
        NSString *partID = [bodyPartDict valueForKey:@"id"];
        NSString *picker =  [[NSString alloc] initWithFormat:@"%@%@",
                             mediaWSAddress, [bodyPartDict valueForKey:@"picker"]];
        NSString *file = [[NSString alloc] initWithFormat:@"%@%@",
                          mediaWSAddress, [bodyPartDict valueForKey:@"file"]];
        
        BotPart *botPart = [[BotPart alloc] initWithPartID:partID withPicker:picker withFile:file];
        [partsMutArray addObject:botPart];
    }
    
    return partsMutArray;
}

@end

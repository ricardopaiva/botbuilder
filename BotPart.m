//
//  BotBodyPart.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "BotPart.h"

@implementation BotPart

- (id)initWithPartID:(NSString *)partID withPicker:(NSString *)picker withFile:(NSString *)file
{
    self = [super init];
    
    if (self) {
        [self setPartID:partID];
        [self setFile:file];
        [self setPicker:picker];
    }
    
    return self;
}

@end

//
//  LoginManager.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "LoginManager.h"

@implementation LoginManager
{
    LogState logState;
}

#pragma mark - Setters and Getters

-(LogState)getLogState
{
    return logState;
}

#pragma mark - Functions

-(void)logout
{
    self.token = nil;
    self.userID = nil;
    logState = STATE_LOGGEDOUT;
}

-(void)loginWithUser:(NSString *)user andPassword:(NSString *)pass onWebservice:(WebServiceHelper *)ws
{
    NSURLRequest *req = [ws requestAuthTokenForUser:user withPass:pass];
    
    DataDownloader *downloader = [[DataDownloader alloc] initWithRequest:req delegate:self];
    [downloader startDownload];
}

#pragma mark - DataDownloader Delegate methods

-(void)didFinishedDownloadData:(NSData *)data
{
    NSError *error;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    
    if (error) {
        [self didFailedDownloadData:error];
        return;
    }
    
    if ([json objectForKey:@"error"]) {
        NSError *error = [[NSError alloc] initWithDomain:@"auth failed" code:0 userInfo:nil];
        [self didFailedDownloadData:error];
        return;
    }
    
    self.token = [json objectForKey:@"token"];
    self.userID = [json objectForKey:@"uid"];
    logState = STATE_LOGGEDIN;
  
    if([self.delegate respondsToSelector:@selector(didChangeLogState:)]) {
        [self.delegate didChangeLogState:logState];
    }
}

-(void)didFailedDownloadData:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(didFailedLogin:)]) {
        [self.delegate didFailedLogin:error];
    }
}

@end

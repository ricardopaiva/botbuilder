//
//  BotBodyPart.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BotPart : NSObject

@property (strong, nonatomic) NSString *partID;
@property (strong, nonatomic) NSString *picker;
@property (strong, nonatomic) NSString *file;

- (id)initWithPartID:(NSString *)partID withPicker:(NSString *)picker withFile:(NSString *)file;

@end

//
//  UserInfo.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 12/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo

- (id)initWithUserID:(NSString *)userID withNickname:(NSString *)nickname withAvatarImgURL:(NSString *)avatarImgURL withTwitter:(NSString *)twitter withName:(NSString *)name
{
    self = [super init];
    if (self) {
        [self setUserID:userID];
        [self setNickname:nickname];
        [self setAvatarImgURL:avatarImgURL];
        [self setTwitter:twitter];
        [self setName:name];
    }
    return self;
}

@end

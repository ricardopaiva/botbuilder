//
//  Utils.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 10/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString*)encodeURL:(NSString *)string
{
    NSString *newString = (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, NULL, CFSTR(":/?#[]@!$ &'()*+,;=\"<>%{}|\\^~`"), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    if (newString)
    {
        return newString;
    }
    
    return @"";
}

+ (NSString *) cacheDirectoryPath
{
    NSArray *cachesArray = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = [cachesArray objectAtIndex:0];
    
    return cacheDirectory;
}

+ (NSString *) documentDirectoryPath
{
    NSArray *docArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docDirectory = [docArray objectAtIndex:0];
    
    return docDirectory;
}

+ (NSString *) bundlePath
{
    NSString *bundlePath = [[NSBundle mainBundle] bundlePath];
    return bundlePath;
}

+ (BOOL) createEmptyFileAtPath:(NSString *) path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        NSError *error;
        [[NSData data] writeToFile:path options:0 error:&error];
        if (error) {
            NSLog(@"Error creating file: %@", error);
            return NO;
        }
        return YES;
    }
    return NO;
}

+ (BOOL) renameFileAtPath:(NSString *)path toPath:(NSString *)newPath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *fileError;
    if ([fileManager fileExistsAtPath:path]) {
        [fileManager moveItemAtPath:path toPath:newPath error:&fileError];
        
        if (!fileError) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL) fileExistsAtPath:(NSString *)path
{
    return [[NSFileManager defaultManager] fileExistsAtPath:path];
}

+(int) getRandomNumberBetween:(int)from and:(int)to {
    return (int)from + arc4random() % (to-from+1);
}

@end

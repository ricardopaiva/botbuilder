//
//  ClickableWebImageView.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "ClickableWebImageView.h"

@implementation ClickableWebImageView

-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url delegate:(id<ClickableWebImageDelegate>)delegate
{
    return [self initWithFrame:frame withURL:url delegate:delegate operationQueue:nil];
}

-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url delegate:(id<ClickableWebImageDelegate>)delegate operationQueue:(NSOperationQueue *)queue
{
    self = [super initWithFrame:frame withURL:url operationQueue:queue];
    if (self) {
        [self setDelegate:delegate];
        [self setUserInteractionEnabled:YES];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)handleTap:(UITapGestureRecognizer *)sender {
    if (sender.state == UIGestureRecognizerStateEnded)     {
        if ([self.delegate respondsToSelector:@selector(didTapImageWithTag:)]) {
            [self.delegate didTapImageWithTag:self.tag];
        }
    }
}

@end

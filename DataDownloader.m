//
//  DataDownloader.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "DataDownloader.h"

@implementation DataDownloader

- (id)initWithRequest:(NSURLRequest *)request delegate:(id<DataDownloaderDelegate>)delegate
{
    self = [super init];
    
    if (self) {
        [self setDelegate:delegate];
        self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
        self.receivedData = [[NSMutableData alloc] init];
    }
    
    return self;
}

- (void)startDownload
{
    [self.connection start];
}

-(void)stopDownload
{
    [self clean];
}

#pragma mark - NSURLConnection delegate methods

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if (self.delegate) {
        [self.delegate didFinishedDownloadData:self.receivedData];
        
        [self clean];
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if ([self.delegate respondsToSelector:@selector(didFailedDownloadData:)]) {
        [self.delegate didFailedDownloadData:error];
    }
    
    [self clean];
}

- (void) clean
{
    [self.connection cancel];
    [self setConnection:nil];
    [self setReceivedData:nil];
    [self setDelegate:nil];
}

@end

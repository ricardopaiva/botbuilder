//
//  UserInfo.h
//  BotBuilder
//
//  Created by Ricardo Paiva on 12/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *nickname;
@property (strong, nonatomic) NSString *avatarImgURL;
@property (strong, nonatomic) NSString *twitter;
@property (strong, nonatomic) NSString *name;

- (id)initWithUserID:(NSString *)userID withNickname:(NSString *)nickname withAvatarImgURL:(NSString *)avatarImgURL withTwitter:(NSString *)twitter withName:(NSString *)name;

@end

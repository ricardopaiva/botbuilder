//
//  UserInfoLoader.h
//  BotBuilder
//
//  Loader class using simple NSURLConnection
//
//  Created by Ricardo Paiva on 12/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
#import "DataDownloader.h"

@protocol UserInfoLoaderDelegate <NSObject>
@required
- (void) didFinishedLoadingUserInfo:(UserInfo *)userInfo;
@optional
- (void) didFailedLoadingUserInfo:(NSError *)error;
@end

@interface UserInfoLoader : NSObject <DataDownloaderDelegate>
@property (strong, nonatomic) id<UserInfoLoaderDelegate> delegate;
@property (strong, nonatomic) DataDownloader *dataDownloader;

- (id) initWithRequest:(NSURLRequest *)request delegate:(id<UserInfoLoaderDelegate>)delegate;
- (void) startLoading;


@end

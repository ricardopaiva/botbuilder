//
//  WebImageView.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 11/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "WebImageView.h"
#import "Utils.h"

@implementation WebImageView

- (id)initWithFrame:(CGRect)frame withURL:(NSURL *)url
{
    return [self initWithFrame:frame withURL:url operationQueue:nil];
}

-(id)initWithFrame:(CGRect)frame withURL:(NSURL *)url operationQueue:(NSOperationQueue *)queue
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setImageURL:url];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityIndicator.center = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
        
        //If there's a local copy of the image, it is loaded.
        NSString *imageFilename = url.lastPathComponent;
        NSString *cacheImageFileFullPath = [[Utils cacheDirectoryPath] stringByAppendingPathComponent:imageFilename];
        if ([Utils fileExistsAtPath:cacheImageFileFullPath]) {
            [self setImage:[UIImage imageWithContentsOfFile:cacheImageFileFullPath]];
        } else {
            
            [self showActivityIndicator];
            
            self.imgFileDownloader = [[FileDownloaderOperation alloc] initWithURL:url];
            [self.imgFileDownloader addObserver:self forKeyPath:@"isFinished" options:NSKeyValueObservingOptionNew context:NULL];
            [queue addOperation:self.imgFileDownloader]; // operation starts as soon as its added
        
        }
    }
    return self;
}

#pragma mark - Activity Indicator methods

-(void) showActivityIndicator
{
    [self.activityIndicator startAnimating];
//    [self.activityIndicator setCenter:self.center];
    
    [self addSubview:self.activityIndicator];
}

-(void) hideActivityIndicator
{
    [self.activityIndicator removeFromSuperview];
}

@end

//
//  WSImageView.m
//  BotBuilder
//
//  Created by Ricardo Paiva on 12/05/13.
//  Copyright (c) 2013 Ricardo Paiva. All rights reserved.
//

#import "WSImageView.h"

@implementation WSImageView

- (id)initWithFrame:(CGRect)frame withURL:(NSURL *)url
{
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setImageURL:url];
        
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        [self showActivityIndicator];
        
        NSURLRequest *imageRequest = [[NSURLRequest alloc] initWithURL:url];
        self.dataDownloader = [[DataDownloader alloc] initWithRequest:imageRequest delegate:self];
        [self.dataDownloader startDownload];
    }
    return self;
}

#pragma mark - DataDownloader Delegate methods

-(void)didFinishedDownloadData:(NSData *)data
{
    [self hideActivityIndicator];
    [self setImage:[UIImage imageWithData:data]];
}

-(void)didFailedDownloadData:(NSError *)error
{
    [self hideActivityIndicator];
}

#pragma mark - Activity Indicator methods

-(void) showActivityIndicator
{
    [self.activityIndicator startAnimating];
    [self.activityIndicator setCenter:self.center];
    
    [self addSubview:self.activityIndicator];
}

-(void) hideActivityIndicator
{
    [self.activityIndicator removeFromSuperview];
}

@end
